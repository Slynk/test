package com.slynk.transloctest.activities;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.koushikdutta.async.future.FutureCallback;
import com.slynk.transloctest.R;
import com.slynk.transloctest.library.IPCrawler;
import com.slynk.transloctest.library.map.IPMap;
import com.slynk.transloctest.library.network.ip.IterableIPV4;
import com.slynk.transloctest.library.tagging.Tags;
import com.slynk.transloctest.network.models.IPInfo;

public class MainActivity extends FragmentActivity {
    private ProgressBar spinner;
    private EditText txtStartingIP;
    private EditText txtEndingIP;
    private Button btnScan;
    private IPMap map;

    private IPCrawler crawler;

    private boolean isScanning = false;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (ProgressBar) findViewById(R.id.activity_main_progress);
        txtStartingIP = (EditText) findViewById(R.id.activity_main_starting_ip);
        txtEndingIP = (EditText) findViewById(R.id.activity_main_ending_ip);
        btnScan = (Button) findViewById(R.id.activity_main_scan);

        btnScan.setOnClickListener(SCAN_CLICK);

        final SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_main_map);
        mapFragment.getMapAsync(MAP_READY_CALLBACK);
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLoadingIPs();
    }

    private void startLoadingIPs(final IterableIPV4 start, final IterableIPV4 end) {
        isScanning = true;

        btnScan.setText("Stop");
        spinner.setVisibility(View.VISIBLE);
        crawler = new IPCrawler(getApplicationContext(), start, end);
        crawler.start(DATA_RECEIVED_CALLBACK, ON_SCAN_COMPLETE);

    }

    private void stopLoadingIPs() {
        isScanning = false;

        btnScan.setText("Scan");
        spinner.setVisibility(View.INVISIBLE);
        if(crawler != null) {
            crawler.stop();
            Toast.makeText(getApplicationContext(), "Scan complete!", Toast.LENGTH_LONG).show();
        }
    }

    private final View.OnClickListener SCAN_CLICK = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            if(isScanning) {
                stopLoadingIPs();
                return;
            }

            // Clean up before we start scanning
            spinner.setVisibility(View.INVISIBLE);
            map.clear();
            if(crawler != null) {
                crawler.stop();
            }

            // Validate the entered ip addresses
            final String startingIP = txtStartingIP.getText().toString().trim();
            final String endingIP = txtEndingIP.getText().toString().trim();

            if(startingIP.isEmpty() || endingIP.isEmpty()) {
                Toast.makeText(getApplicationContext(), "Please enter both a starting ip and an ending ip.", Toast.LENGTH_SHORT).show();
                return;
            }

            final IterableIPV4 start = IterableIPV4.getByName(startingIP);
            final IterableIPV4 end = IterableIPV4.getByName(endingIP);
            if(start == null && end == null) {
                Toast.makeText(getApplicationContext(), "Both the starting ip and ending ip are invalid!!", Toast.LENGTH_LONG).show();
            } else if(start == null) {
                Toast.makeText(getApplicationContext(), "The starting ip address entered is invalid!", Toast.LENGTH_LONG).show();
            } else if(end == null) {
                Toast.makeText(getApplicationContext(), "The ending ip address entered is invalid!", Toast.LENGTH_LONG).show();
            } else {
                startLoadingIPs(start, end);
            }
        }
    };

    private final Runnable ON_SCAN_COMPLETE = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    stopLoadingIPs();
                }
            });
        }
    };

    private final OnMapReadyCallback MAP_READY_CALLBACK = new OnMapReadyCallback() {
        @Override
        public void onMapReady(final GoogleMap googleMap) {
            map = new IPMap(getApplicationContext(), googleMap);
            btnScan.setEnabled(true);
        }
    };

    private final FutureCallback<IPInfo> DATA_RECEIVED_CALLBACK = new FutureCallback<IPInfo>() {
        @Override
        public void onCompleted(final Exception e, final IPInfo result) {
            // Check that we have a result and no exception.
            if(e != null) {
                e.printStackTrace();
            } else {
                if(map == null) {
                    Log.w(Tags.MAIN, "Marker data received before map was ready!");
                } else if(result == null) {
                    Log.w(Tags.MAIN, "Null result on DATA_RECEIVED_CALLBACK without an exception!");
                } else {
                    map.markIP(IterableIPV4.getByName(result.ipAddress), Double.parseDouble(result.latitude), Double.parseDouble(result.longitude));
                }
            }
        }
    };
}
