package com.slynk.transloctest.library.map;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.slynk.transloctest.library.network.ip.IterableIPV4;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by Slynk on 7/8/2015.
 */
public class IPMap {
    private final Context context;
    private final GoogleMap map;

    private HashMap<String, Marker> markerCache = new HashMap<>();
    private HashMap<String, IterableIPV4[]> rangeCache = new HashMap<>();
    private final IterableIPV4[] sortCache = new IterableIPV4[3]; // Avoid object allocations for sorting by reusing the array.

    public IPMap(final Context context, final GoogleMap map) {
        this.context = context;
        this.map = map;
    }

    public synchronized void markIP(final IterableIPV4 ip, final double lat, final double lon) {
        final String key = lat + "," + lon;
        final Marker marker = markerCache.get(key);

        // If there isn't a map marker, create one. Else, just update the marker.
        if (marker == null) {
            // Create Marker
            final LatLng loc = new LatLng(lat, lon);
            final Marker toAdd =  map.addMarker(new MarkerOptions().position(loc).title(ip.toString()));

            // Add to cache
            markerCache.put(key, toAdd);
            rangeCache.put(key, new IterableIPV4[]{ip, null});

            // Update view
            map.moveCamera(CameraUpdateFactory.newLatLng(loc));
            toAdd.showInfoWindow();
            Toast.makeText(context, "New location found!", Toast.LENGTH_LONG).show();
        } else {
            final IterableIPV4[] range = rangeCache.get(key);
            if(range[1] == null) { // if only the first ip in the range has been determined.
                final int comparison = ip.compareTo(range[0]);
                if(comparison < 0) { // if less than the only ip in the range then move and add.
                    range[1] = range[0];
                    range[0] = ip;
                } else if(comparison > 0) { // if greater than, then put at the end.
                    range[1] = ip;
                } else {
                    return; // Identical if, so just ignore
                }
            } else { // Sort all three and choose the highest and lowest.
                sortCache[0] = range[0];
                sortCache[1] = range[1];
                sortCache[2] = ip;
                Arrays.sort(sortCache);
                range[0] = sortCache[0];
                range[1] = sortCache[2];
            }

            // Update marker
            marker.setTitle(range[0] + " to " + range[1]);
            marker.showInfoWindow();
        }
    }

    public void clear() {
        map.clear();
        markerCache.clear();
    }
}
