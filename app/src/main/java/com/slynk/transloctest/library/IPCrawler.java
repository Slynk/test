package com.slynk.transloctest.library;

import android.content.Context;
import android.util.Log;

import com.koushikdutta.async.future.FutureCallback;
import com.slynk.transloctest.library.network.ip.IterableIPV4;
import com.slynk.transloctest.library.tagging.Tags;
import com.slynk.transloctest.network.controllers.IPInfoController;
import com.slynk.transloctest.network.models.IPInfo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Slynk on 7/8/2015.
 */
public class IPCrawler {
    private IterableIPV4 currentIP;
    private IterableIPV4 endingIP;

    private Thread thread;
    private ExecutorService pool = Executors.newCachedThreadPool();

    private final Context context;

    public IPCrawler(final Context context, final IterableIPV4 startingIP, final IterableIPV4 endingIP) {
        this.context = context;

        if(startingIP.compareTo(endingIP) <= 0) {
            this.currentIP = startingIP;
            this.endingIP = endingIP;
        } else {
            this.currentIP = endingIP;
            this.endingIP = startingIP;
        }
    }

    public synchronized void start(final FutureCallback<IPInfo> callback, final Runnable onComplete) {
        if(thread != null && thread.isAlive()) {
            thread.interrupt();
        }

        // ipinfodb only supports 2 requests per second, so only fire off 2 requests per 1.2 seconds.
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(currentIP.compareTo(endingIP) <= 0) {

                    final String ip = currentIP.toString();
                    try {
                        pool.submit(new Runnable() {
                            @Override
                            public void run() {
                                Log.v(Tags.MAIN, "Testing " + ip);
                                IPInfoController.getCityForIp(context, ip, callback);
                            }
                        });
                    } catch (final Exception e) {
                        // In case the pool has been shutdown mid thread
                        e.printStackTrace();
                    }

                    currentIP.increment();

                    try{
                        Thread.sleep(600);
                    }catch(final InterruptedException e) {
                        Log.e(Tags.MAIN, "Interrupting...");
                        Thread.currentThread().interrupt();
                        return;
                    }
                }
                Log.v(Tags.MAIN, "Testing complete.");
                if(onComplete != null) {
                    onComplete.run();
                }
            }
        });
        thread.start();
    }

    public void stop() {
        if(thread != null && thread.isAlive()) {
            thread.interrupt();
        }

        pool.shutdownNow();
    }
}
