package com.slynk.transloctest.library.network.ip;

import org.apache.commons.validator.routines.InetAddressValidator;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by Slynk on 7/8/2015.
 *
 * IterableIPV4 is a wrapper for Inet4Address to encompass the functionality of incrementing an address.
 */
public class IterableIPV4 implements Comparable<IterableIPV4> {
    private static final InetAddressValidator validator = InetAddressValidator.getInstance();
    private byte[] address;

    public IterableIPV4(final InetAddress address) {
        this.address = address.getAddress();
    }

    // Returns an IterableIPV4 if the passed ip is a valid raw ipv4 string
    public static IterableIPV4 getByName(final String ip) {
        try {
            if(validator.isValidInet4Address(ip)) { // Avoid dns lookup if invalid raw ip
                return new IterableIPV4(Inet4Address.getByName(ip));
            } else {
                return null;
            }
        } catch(final UnknownHostException uhe) { // should never occur, but just in case
            uhe.printStackTrace();
            return null;
        }
    }

    public void increment() {;
        incrementRecursive(3, address);
    }

    private void incrementRecursive(final int index, final byte[] array){
        if(index >= 0) {
            if((array[index]& 0xff) == 255) {
                array[index] = 0;
                incrementRecursive(index-1, array);
            } else {
                array[index]++;
            }
        }
    }

    // Copied from: http://stackoverflow.com/questions/4256438/calculate-whether-an-ip-address-is-in-a-specified-range-in-java#4256603
    public long toLong() {
        long result = 0;
        for (final byte octet : address) {
            result <<= 8;
            result |= octet & 0xff;
        }
        return result;
    }

    @Override
    public String toString() {
        return (address[0] & 0xff) + "." + (address[1] & 0xff) + "." + (address[2] & 0xff) + "." + (address[3] & 0xff);
    }

    @Override
    public int compareTo(final IterableIPV4 another) {
        final long low = toLong();
        final long hi = another.toLong();

        return low == hi ? 0 : (low < hi ? -1 : 1);
    }
}
