package com.slynk.transloctest.network.controllers;

import android.content.Context;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.slynk.transloctest.network.models.IPInfo;

/**
 * Created by Slynk on 7/7/2015.
 */
public class IPInfoController {
    private static final String BASE_URL = "http://api.ipinfodb.com/v3/";
    private static final String API_KEY = "b910a5ee29e53f7b4a58c4b47de9e0508a0336292861e415b297ec8643f764b2";

    public static void getCityForIp(final Context context, final String ip, final FutureCallback<IPInfo> callback) {
        Ion.with(context)
                .load("GET", BASE_URL + "ip-city/")
                .setTimeout(60000)
                .addQuery("key", API_KEY)
                .addQuery("ip", ip)
                .addQuery("format", "json")
                .as(IPInfo.class)
                .setCallback(callback);
    }
}
