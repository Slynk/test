package com.slynk.transloctest.network.models;

/**
 * Created by Slynk on 7/7/2015.
 *
 * IPInfo represents the json structure returned by the ipinfodb api.
 */
public class IPInfo {
    public String ipAddress;
    public String latitude;
    public String longitude;
}
